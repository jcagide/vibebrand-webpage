function UserService() {
	var self = this;
	this.delegate;
	function sendEmail(dataToSend){
		jQuery.ajax({
		  url: '/sendMail',
		  type: 'POST',
		  dataType: 'json',
		  data: dataToSend,
		  complete: function(xhr, textStatus) {
		   	self.delegate.enableEvents();
		  },
		  success: function(data, textStatus, xhr) {
		  	if(data.result === true)
		  		self.delegate.onSuccessfulSend();
		  	else
		  		self.delegate.onFailedSend();
		  },
		  error: function(xhr, textStatus, errorThrown) {
		  	self.delegate.onFailedSend();
		  }
		});
		
	};
	this.sendEmail = sendEmail;
};