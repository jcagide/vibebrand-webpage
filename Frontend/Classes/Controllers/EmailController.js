function EmailController() {
	var self = this;
	this.delegate;
	this.validation;
	this.messages;
	function createContent(){
		enableEvents();
	};
	function createLoader(){
		var container = $('div.contact');
		var sendBtn   = container.find('.send-button');
		var loader    = $('<div class="loader"></div>');
		var image     = $('<img/>');
		var message   = $('<span></span>');
		loader.append(image);
		loader.append(message);
		image.attr('src', '');
		message.text('Enviando');
		sendBtn.before(loader);
	};
	function removeLoader(){
		var container = $('div.contact');
		container.find('.loader').remove();
	};
	function onSuccessfulSend(){
		var container = $('div.contact');
		var sendBtn= container.find('.send-button');
		sendBtn.fadeOut('fast', sendBtn.remove);
		self.messages.createMessage.call(container, {
			message: "Correo enviado correctamete.",
			className: "success-message",
			delay: 2500
		});
		removeLoader();
	};
	function onFailedSend(){
		var container = $('div.contact');
		self.messages.createMessage.call(container, {
			message: "No se pudo enviar el correo, por favor intente de nuevo más tarde.",
			className: "error-message",
			delay: 2500
		});
		removeLoader();
	};
	//Validation
	function validateForm(){
		var count     = 0;
		var container = $('div.contact');
		var cname     = container.find('.name-input input');
		var email     = container.find('.email-input input');
		var phone     = container.find('.phone-input input');
		var textarea  = container.find('.message-input textarea');

		if(!self.validation.validateWithPattern(cname.val() ,null, false)){
			count++;
			self.messages.createMessage.call(cname.parents('*[class$="input"]') , {
				message: "Por favor ingrese su nombre.",
				className: "error-message"
			});
		};
		if(!self.validation.validateWithPattern(textarea.val() ,null, false)){
			count++;
			self.messages.createMessage.call(textarea.parents('*[class$="input"]'), {
				message: "Por favor ingrese un mensaje.",
				className: "error-message"
			});
		};
		if(!self.validation.validatePhone(phone.val(), false)){
			count++;
			self.messages.createMessage.call(phone.parents('*[class$="input"]'), {
				message: "Teléfono no válido.",
				className: "error-message"
			});
		};
		if(!self.validation.validateEmail(email.val(), false)){
			count++;
			self.messages.createMessage.call(email.parents('*[class$="input"]'), {
				message: "Correo electrónico no válido.",
				className: "error-message"
			});
		};
		return count < 1;
	};
	//Data
	function createEmailData(){
		var container = $('div.contact');
		var dataToSend = {
			name 	:$.trim(container.find('.name-input input').val()),
			email 	:$.trim(container.find('.email-input input').val()),
			phone 	:$.trim(container.find('.phone-input input').val()),
			message :$.trim(container.find('.message-input textarea').val())
		};
		return JSON.stringify(dataToSend);
	};
	//Events
	function onClickSend(){
		if(validateForm()){
			disableEvents();
			createLoader();
			self.delegate.sendEmail({emailData: createEmailData()});
		}
	};
	function onFocusInput(){
		var input = $(this);
		var container = $(this).parents('*[class$="-input"]');
		container.find('.error-message').remove();
	};
	//Disable Enable
	function disableEvents(){
		var container = $('div.contact');
		var sendBtn   = container.find('.send-button');
		sendBtn.unbind('click');
	};
	function enableEvents(){
		var container = $('div.contact');
		var sendBtn   = container.find('.send-button');
		var inputs = container.find('input,textarea');
		sendBtn.unbind('click');
		inputs.unbind('click');
		sendBtn.bind('click', onClickSend);
		inputs.bind('focusin', onFocusInput);
	};
	this.disableEvents    = disableEvents;
	this.enableEvents     = enableEvents;
	this.createContent    = createContent;
	this.onSuccessfulSend = onSuccessfulSend;
	this.onFailedSend     = onFailedSend;
};