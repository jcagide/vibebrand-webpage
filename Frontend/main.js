Importer.importfile('Classes/Services/UserService.js');
Importer.importfile('Classes/Services/TextValidation.js');
Importer.importfile('Classes/Controllers/EmailController.js');
Importer.importfile('Classes/Controllers/MessageController.js');

$(document).ready(function(){Importer.loadImports(main);});
function main(){
	var emailController        = new EmailController();
	var userService            = new UserService();
	var messageController      = new MessageController();
	
	emailController.delegate   = userService;
	emailController.validation = new TextValidation();
	emailController.messages   = messageController;
	
	userService.delegate       = emailController;
	
	emailController.createContent();
};