var util        = require('util');
var http        = require('http');
var mongo       = require('mongodb');
var express     = require('express');
var nodemailer  = require("nodemailer");
var queryString = require('querystring');


var app      = express();
var webroot  = '/Frontend';
var port     = process.env.PORT || 1337;
var mongoUri =  process.env.MONGOLAB_URI ||
                process.env.MONGOHQ_URL ||
                'mongodb://localhost/test';
mongoUri += "?safe=true&strict=false";

function insert(col, value, callback) {
    mongo.Db.connect(mongoUri, function (err, db) {
      db.collection(col, function(er, collection) {
        collection.insert(value, function(err, result){
            callback(err, result);
        });
      });
    });
};
function get(col, callback) {
    mongo.Db.connect(mongoUri, function (err, db) {
      db.collection(col, function(er, collection) {
        collection.find().toArray(function(err, items){
            callback(err, items);
        });
      });
    });
};
function start() {
    app.use(express.static(__dirname + webroot));
    app.get("/insert/:col/:value", function(req, res, next){
        console.log(req.params);
        res.send(util.format("Insertando %s en %s", req.params.col, req.params.value));
        //callback = function(error, result){
            //use error and result
        //};
        //insert(req.params.col, {"value": req.params.value}, callback);
    });
    app.get("/get/:col", function(req, res, next){
        callback = function(error, items){
            res.send({"result": items});
        };
        get(req.params.col, callback);
    });
    app.post("/sendMail", function(req, res, next){
        var fullBody = '';
        req.on('data', function(data){
           fullBody+= data.toString();
        });
        req.on('end', function() {
            var protoData =  queryString.parse(fullBody);
            var emailData = {};
            if(typeof protoData == "object" && typeof protoData.emailData != "undefined")
                emailData = JSON.parse(protoData.emailData);
            sendMail(emailData, res)
        });
        
    });
    app.listen(port, '0.0.0.0');
    console.log('Server running');
};
function sendMail(emailData, res){
    var smtpTransport = nodemailer.createTransport("SMTP",{
            host: "gator109.hostgator.com", // hostname
            secureConnection: true, // use SSL
            port: 465, // port for secure SMTP
            auth: {
                user: "sender@vibebrand.com",
                pass: "-@$#HTMZVIOQ"
            }
    });
    var mailOptions = {
        from: "Vibebrand ✔ <sender@vibebrand.com>", // sender address
        to: "info@vibebrand.com", // list of receivers
        subject: "Test ✔", // Subject line
        text: "Email:"+emailData.email+" Teléfono :"+emailData.phone+" Mensaje: "+emailData.message, // plaintext body
        html: "<b>Email:"+emailData.email+" Teléfono :"+emailData.phone+" Mensaje: "+emailData.message+"</b>" // html body
    }
    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
            var resposeData = JSON.stringify({result: false});
            res.write(resposeData);
        }else{
            var resposeData = JSON.stringify({result: true});
            res.write(resposeData);
        }
        res.send();
    });
};
exports.start = start;
